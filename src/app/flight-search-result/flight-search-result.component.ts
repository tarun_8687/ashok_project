import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FlightModel } from './../interface/flight-model';
import { FlightService } from './../service/flight.service';

// To create to and fro flight combinations
export interface combo {
  flight1: FlightModel,
  flight2: FlightModel
}

@Component({
  selector: 'app-flight-search-result',
  templateUrl: './flight-search-result.component.html',
  styleUrls: ['./flight-search-result.component.css']
})
export class FlightSearchResultComponent implements OnInit {

  //@Input() flightResults: FlightModel;
  @Input() tripType: boolean; //One way or round trip based on tab selected
  @Input() lower: number; //Lower limit of price range
  @Input() upper: number; //Upper limit of price range
  toFlights: FlightModel[] = [];
  froFlights: FlightModel[] = [];
  resultArr: combo[] = [];

  _flightResults: FlightModel;
get flightResults(): FlightModel {
    return this._flightResults;
}

@Input('flightResults')
set flightResults(value: FlightModel) {
    this._flightResults = value;
    this.resultArr =[];
    this.updateFlight()
}

  constructor(private _flightService: FlightService) { }

  ngOnInit() {
    
    
  }

  updateFlight()
  {
    debugger;
    this.toFlights = this._flightResults[0];
    this.froFlights = this._flightResults[1];
    for (let i = 0; i < this._flightResults[0].length; i++) {
      if (JSON.stringify(this._flightResults[1]) !== `[]`) {
        for (let j = 0; j < this._flightResults[1].length; j++) {
          if (this._flightResults[0][i].FareDetails + this._flightResults[1][j].FareDetails >= this.lower
            && this._flightResults[0][i].FareDetails + this._flightResults[1][j].FareDetails <= this.upper) {
            this.resultArr.push({ flight1: this._flightResults[0][i], flight2: this._flightResults[1][j] });
          }
        }
      } else {
        this.resultArr.push({ flight1: this._flightResults[0][i], flight2: null });
      }
    }
  }
}
