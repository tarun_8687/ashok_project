import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse, HttpRequest } from '@angular/common/http';

import { FlightModel } from './../interface/flight-model';
import { DatePipe } from '@angular/common'


@Injectable()
export class FlightService {
  allFlights: FlightModel[] = [];
  flightResults: Array<FlightModel[]> = [];
  toFlightResults: FlightModel[] = [];
  froFlightResults: FlightModel[] = [];
  flightsUrl = 'api/flights';  // URL to fetch details from
  deptDt1: string;
  deptDt2: string;

  constructor( public datepipe: DatePipe) { }

  /** GET flight details from the server */
  getFlightDetails(flt: FlightModel[], lower, upper): Array<FlightModel[]> {
    this.flightResults = [];
    this.toFlightResults = [];
    this.froFlightResults = [];


    
    this.allFlights = this.getMockData();
    // f=0 means it has departure date of to flight
    // f=1 means it has dept date of fro flight
    for (let f = 0; f < flt.length; f++) {
      for (let i = 0; i < this.allFlights.length; i++) {
        let od1 = this.allFlights[i].OriginDestination;
        let od2 = flt[f].OriginDestination;
        // Transforming date values to ease comparison
        this.deptDt1 = this.datepipe.transform(od1.DeptDate, 'yyyy-MM-dd');
        this.deptDt2 = this.datepipe.transform(od2.DeptDate, 'yyyy-MM-dd');
        // Check conditions that satisfy user input to flight details in DB
        if (od1.Origin == od2.Origin && od1.Destination == od2.Destination &&
          this.deptDt1 === this.deptDt2 && this.allFlights[i].SeatsAvailable > flt[f].SeatsAvailable
          && this.allFlights[i].FareDetails >= lower && this.allFlights[i].FareDetails <= upper) {
          if (f === 0) {
            this.toFlightResults.push(this.allFlights[i]);
          } else {
            this.froFlightResults.push(this.allFlights[i]);
          }
        }
      }
    }
    this.flightResults[0] = this.toFlightResults;
    this.flightResults[1] = this.froFlightResults;

    // Returns combined to flight and fro flight details
    return this.flightResults;
  }

  getMockData() {
    const flights: FlightModel[] = [
      {
        "id": 1,
        "FlightCode": "AI-101",
        "SeatsAvailable": 3,
        "OriginDestination": { "Origin": "MAA", "Destination": "BLR", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 03:00 PM") },
        "FareDetails": 4000
      },
      {
        "id": 2,
        "FlightCode": "AI-560",
        "SeatsAvailable": 6,
        "OriginDestination": { "Origin": "BLR", "Destination": "MAA", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 04:00 AM") },
        "FareDetails": 2500
      },
      {
        "id": 3,
        "FlightCode": "AI-202",
        "SeatsAvailable": 5,
        "OriginDestination": { "Origin": "MAA", "Destination": "BLR", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 03:00 PM") },
        "FareDetails": 2000
      },
      {
        "id": 4,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "MAA", "Destination": "BLR", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 02:00 PM") },
        "FareDetails": 2000
      },
      {
        "id": 5,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "DEL", "Destination": "HYD", "DeptDate": new Date("23 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 02:00 PM") },
        "FareDetails": 6000
      },
      {
        "id": 6,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "HYD", "Destination": "DEL", "DeptDate": new Date("23 Sep, 2019 01:00 PM"), "ArrDate": new Date("25 Sep, 2019 1.30:00 PM") },
        "FareDetails": 6500
      },
      {
        "id": 7,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "BLR", "Destination": "DEL", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 02:00 PM") },
        "FareDetails": 6000
      },
      {
        "id": 8,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "BLR", "Destination": "DEL", "DeptDate": new Date("25 Sep, 2019 02:00 PM"), "ArrDate": new Date("25 Sep, 2019 02:00 PM") },
        "FareDetails": 4000
      },
      {
        "id": 9,
        "FlightCode": "AI-300",
        "SeatsAvailable": 10,
        "OriginDestination": { "Origin": "HYD", "Destination": "DEL", "DeptDate": new Date("25 Sep, 2019 01:00 PM"), "ArrDate": new Date("25 Sep, 2019 1.30:00 PM") },
        "FareDetails": 6500
      }
    ];
    return flights;
  }

}